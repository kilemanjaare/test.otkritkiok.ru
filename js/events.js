
jQuery(".closePopUpLink").live('click', function() {
		jQuery(this).parent().fadeOut();
		jQuery(".overlay").fadeOut();
	});
	
jQuery(".feedback, .individ6 .slink, .feedOther").live('click', function() {
		jQuery(".overlay, #callBack").fadeIn();
	});

jQuery("#st1").live('click', function() {
		var Top_modal_window = (jQuery(document).scrollTop() + 200);
		jQuery(".overlay").fadeIn();
		jQuery("#product1").fadeIn().css({
			"top" : Top_modal_window + "px"
		});
	});
	
jQuery("#st2").live('click', function() {
		var Top_modal_window = (jQuery(document).scrollTop() + 200);
		jQuery(".overlay").fadeIn();
		jQuery("#product2").fadeIn().css({
			"top" : Top_modal_window + "px"
		});
	});
	
jQuery("#st3").live('click', function() {
		var Top_modal_window = (jQuery(document).scrollTop() + 200);
		jQuery(".overlay").fadeIn();
		jQuery("#product3").fadeIn().css({
			"top" : Top_modal_window + "px"
		});
	});
	
jQuery("#st4").live('click', function() {
		var Top_modal_window = (jQuery(document).scrollTop() + 200);
		jQuery(".overlay").fadeIn();
		jQuery("#product4").fadeIn().css({
			"top" : Top_modal_window + "px"
		});
	});
	
jQuery("#st5").live('click', function() {
		var Top_modal_window = (jQuery(document).scrollTop() + 200);
		jQuery(".overlay").fadeIn();
		jQuery("#product5").fadeIn().css({
			"top" : Top_modal_window + "px"
		});
	});
	
jQuery("#st6").live('click', function() {
		var Top_modal_window = (jQuery(document).scrollTop() + 200);
		jQuery(".overlay").fadeIn();
		jQuery("#product6").fadeIn().css({
			"top" : Top_modal_window + "px"
		});
	});
	
jQuery(document).ready(function($) {

// COUNTDOWN BEGIN
jQuery(function(){	
	var ts = new Date(2012, 0, 1)	
	if((new Date()) > ts){
		ts = (new Date(2020, 6, 1));//.getTime() + 16*24*60*60*1000;
		newYear = false;
	}		
	jQuery('.countdown').countdown({
		timestamp	: ts		
	});		
	jQuery('.countdown1').countdown({
		timestamp	: ts		
	});	
});
// COUNTDOWN END

// LICENSES BIG BEGIN
jQuery(".fancybox").fancybox({
	openEffect	: 'none',
	closeEffect	: 'none'
});
// LICENSES BIG END



});
$(function(){
	
	var note = $('#note'),
		ts = new Date(2014, 0, 1),
		newYear = true;
	
	if((new Date()) > ts){
		ts = (new Date()).getTime() + 1*24*60*60*1000;
		newYear = false;
	}
		
	$('#countdown').countdown({
		timestamp	: ts,
		callback	: function(days, hours, minutes, seconds){
			
			var message = "";
			
			message += days + " day" + ( days==1 ? '':'s' ) + ", ";
			message += hours + " hour" + ( hours==1 ? '':'s' ) + ", ";
			message += minutes + " minute" + ( minutes==1 ? '':'s' ) + " and ";
			message += seconds + " second" + ( seconds==1 ? '':'s' ) + " <br />";
			
			if(newYear){
				message += "left until the new year!";
			}
			else {
				message += "left to 10 days from now!";
			}
			
			note.html(message);
		}
	});
	
});
