//This function sends all emails
function vpb_send_email_callback() 
{
	var a = $("#callBack_callback");
	//Variables declaration
	var name = a.find('.name').val();
	var phonet = a.find('.phonet').val();
	var phonen = a.find('.phonen').val();
	
	//Validation process begins
	if( name == "" )
	{
		a.find('.name').css('box-shadow', '0 0 10px red inset');
		a.find('.name').css('border', '1px solid red');
		a.find(".name").focus();
		return false;
	}
	
	else if( phonet == "" || phonen == "" )
	{
		
		if( phonen == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonen').css('box-shadow', '0 0 10px red inset');
			a.find('.phonen').css('border', '1px solid red');
			a.find(".phonen").focus();
		} else {
			a.find('.phonen').removeAttr('style');
		}
		
		if( phonet == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonet').css('box-shadow', '0 0 10px red inset');
			a.find('.phonet').css('border', '1px solid red');
			a.find(".phonet").focus();
		} else {
			a.find('.phonet').removeAttr('style');
		}
	}
	else
	{
		a.find('.name').removeAttr('style');
		a.find('.phonet, .phonen').removeAttr('style');
		var dataString = "name=" + name +  "&phonet=" + phonet +  "&phonen=" + phonen;
		
		$.ajax({
			type: "POST",
			url: "modules/vpb_email_sender_callback.php",
			data: dataString,
			cache: false,
			beforeSend: function() 
			{
				a.parent().find(".miniOverlay").fadeIn();
			},
			success: function(response)
			{
				var response_brought = response.indexOf('vpb_sent');
				if (response_brought != -1) 
				{
					a.each(function(){
						this.reset();
					});
					
					a.find('.name').val('');
					a.find('.phone').val('');					
					jQuery(".feedBackBox").fadeOut();					
					jQuery(".thankBox").fadeIn();			
					
					setTimeout(' jQuery(".thankBox, .overlay").fadeOut();', 5000);					
				}
				else
				{					
					a.each(function(){
						this.reset();
					});					
					a.parent().find(".errorBox, .fileBox i, .miniOverlay").fadeOut();
				}
			}
		});

	}
}

//This function sends all emails
function vpb_send_email_form1() 
{
	var a = $("#callBack_form1");
	//Variables declaration
	var name = a.find('.name').val();
	var phonet = a.find('.phonet').val();
	var phonen = a.find('.phonen').val();
	
	//Validation process begins
	if( name == "" )
	{
		a.find('.name').css('box-shadow', '0 0 10px red inset');
		a.find('.name').css('border', '1px solid red');
		a.find(".name").focus();
		return false;
	}
	
	else if( phonet == "" || phonen == "" )
	{
		
		if( phonen == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonen').css('box-shadow', '0 0 10px red inset');
			a.find('.phonen').css('border', '1px solid red');
			a.find(".phonen").focus();
		} else {
			a.find('.phonen').removeAttr('style');
		}
		
		if( phonet == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonet').css('box-shadow', '0 0 10px red inset');
			a.find('.phonet').css('border', '1px solid red');
			a.find(".phonet").focus();
		} else {
			a.find('.phonet').removeAttr('style');
		}
	}
	else
	{
		a.find('.name').removeAttr('style');
		a.find('.phonet, .phonen').removeAttr('style');
		var dataString = "name=" + name +  "&phonet=" + phonet +  "&phonen=" + phonen;
		
		$.ajax({
			type: "POST",
			url: "modules/vpb_email_sender_form.php",
			data: dataString,
			cache: false,
			beforeSend: function() 
			{
				a.parent().find(".miniOverlay").fadeIn();
			},
			success: function(response)
			{
				var response_brought = response.indexOf('vpb_sent');
				if (response_brought != -1) 
				{
					a.each(function(){
						this.reset();
					});
					
					a.find('.name').val('');
					a.find('.phone').val('');					
					jQuery(".overlay").fadeIn();					
					jQuery(".thankBox").fadeIn();			
					
					setTimeout(' jQuery(".thankBox, .overlay").fadeOut();', 5000);					
				}
				else
				{					
					a.each(function(){
						this.reset();
					});					
					a.parent().find(".errorBox, .fileBox i, .miniOverlay").fadeOut();
				}
			}
		});

	}
}

//This function sends all emails
function vpb_send_email_form2() 
{
	var a = $("#callBack_form2");
	//Variables declaration
	var name = a.find('.name').val();
	var phonet = a.find('.phonet').val();
	var phonen = a.find('.phonen').val();
	
	//Validation process begins
	if( name == "" )
	{
		a.find('.name').css('box-shadow', '0 0 10px red inset');
		a.find('.name').css('border', '1px solid red');
		a.find(".name").focus();
		return false;
	}
	
	else if( phonet == "" || phonen == "" )
	{
		
		if( phonen == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonen').css('box-shadow', '0 0 10px red inset');
			a.find('.phonen').css('border', '1px solid red');
			a.find(".phonen").focus();
		} else {
			a.find('.phonen').removeAttr('style');
		}
		
		if( phonet == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonet').css('box-shadow', '0 0 10px red inset');
			a.find('.phonet').css('border', '1px solid red');
			a.find(".phonet").focus();
		} else {
			a.find('.phonet').removeAttr('style');
		}
	}
	else
	{
		a.find('.name').removeAttr('style');
		a.find('.phonet, .phonen').removeAttr('style');
		var dataString = "name=" + name +  "&phonet=" + phonet +  "&phonen=" + phonen;
		
		$.ajax({
			type: "POST",
			url: "modules/vpb_email_sender_form.php",
			data: dataString,
			cache: false,
			beforeSend: function() 
			{
				a.parent().find(".miniOverlay").fadeIn();
			},
			success: function(response)
			{
				var response_brought = response.indexOf('vpb_sent');
				if (response_brought != -1) 
				{
					a.each(function(){
						this.reset();
					});
					
					a.find('.name').val('');
					a.find('.phone').val('');					
					jQuery(".overlay").fadeIn();					
					jQuery(".thankBox").fadeIn();			
					
					setTimeout(' jQuery(".thankBox, .overlay").fadeOut();', 5000);					
				}
				else
				{					
					a.each(function(){
						this.reset();
					});					
					a.parent().find(".errorBox, .fileBox i, .miniOverlay").fadeOut();
				}
			}
		});

	}
}//This function sends all emails
function vpb_send_email_form3() 
{
	var a = $("#callBack_form3");
	//Variables declaration
	var name = a.find('.name').val();
	var phonet = a.find('.phonet').val();
	var phonen = a.find('.phonen').val();
	
	//Validation process begins
	if( name == "" )
	{
		a.find('.name').css('box-shadow', '0 0 10px red inset');
		a.find('.name').css('border', '1px solid red');
		a.find(".name").focus();
		return false;
	}
	
	else if( phonet == "" || phonen == "" )
	{
		
		if( phonen == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonen').css('box-shadow', '0 0 10px red inset');
			a.find('.phonen').css('border', '1px solid red');
			a.find(".phonen").focus();
		} else {
			a.find('.phonen').removeAttr('style');
		}
		
		if( phonet == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonet').css('box-shadow', '0 0 10px red inset');
			a.find('.phonet').css('border', '1px solid red');
			a.find(".phonet").focus();
		} else {
			a.find('.phonet').removeAttr('style');
		}
	}
	else
	{
		a.find('.name').removeAttr('style');
		a.find('.phonet, .phonen').removeAttr('style');
		var dataString = "name=" + name +  "&phonet=" + phonet +  "&phonen=" + phonen;
		
		$.ajax({
			type: "POST",
			url: "modules/vpb_email_sender_form.php",
			data: dataString,
			cache: false,
			beforeSend: function() 
			{
				a.parent().find(".miniOverlay").fadeIn();
			},
			success: function(response)
			{
				var response_brought = response.indexOf('vpb_sent');
				if (response_brought != -1) 
				{
					a.each(function(){
						this.reset();
					});
					
					a.find('.name').val('');
					a.find('.phone').val('');					
					jQuery(".overlay").fadeIn();					
					jQuery(".thankBox").fadeIn();			
					
					setTimeout(' jQuery(".thankBox, .overlay").fadeOut();', 5000);					
				}
				else
				{					
					a.each(function(){
						this.reset();
					});					
					a.parent().find(".errorBox, .fileBox i, .miniOverlay").fadeOut();
				}
			}
		});

	}
}//This function sends all emails
function vpb_send_email_form4() 
{
	var a = $("#callBack_form4");
	//Variables declaration
	var name = a.find('.name').val();
	var phonet = a.find('.phonet').val();
	var phonen = a.find('.phonen').val();
	
	//Validation process begins
	if( name == "" )
	{
		a.find('.name').css('box-shadow', '0 0 10px red inset');
		a.find('.name').css('border', '1px solid red');
		a.find(".name").focus();
		return false;
	}
	
	else if( phonet == "" || phonen == "" )
	{
		
		if( phonen == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonen').css('box-shadow', '0 0 10px red inset');
			a.find('.phonen').css('border', '1px solid red');
			a.find(".phonen").focus();
		} else {
			a.find('.phonen').removeAttr('style');
		}
		
		if( phonet == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonet').css('box-shadow', '0 0 10px red inset');
			a.find('.phonet').css('border', '1px solid red');
			a.find(".phonet").focus();
		} else {
			a.find('.phonet').removeAttr('style');
		}
	}
	else
	{
		a.find('.name').removeAttr('style');
		a.find('.phonet, .phonen').removeAttr('style');
		var dataString = "name=" + name +  "&phonet=" + phonet +  "&phonen=" + phonen;
		
		$.ajax({
			type: "POST",
			url: "modules/vpb_email_sender_form.php",
			data: dataString,
			cache: false,
			beforeSend: function() 
			{
				a.parent().find(".miniOverlay").fadeIn();
			},
			success: function(response)
			{
				var response_brought = response.indexOf('vpb_sent');
				if (response_brought != -1) 
				{
					a.each(function(){
						this.reset();
					});
					
					a.find('.name').val('');
					a.find('.phone').val('');					
					jQuery(".overlay").fadeIn();					
					jQuery(".thankBox").fadeIn();			
					
					setTimeout(' jQuery(".thankBox, .overlay").fadeOut();', 5000);					
				}
				else
				{					
					a.each(function(){
						this.reset();
					});					
					a.parent().find(".errorBox, .fileBox i, .miniOverlay").fadeOut();
				}
			}
		});

	}
}

//This function sends all emails
function vpb_send_email_items() 
{
	var a = $("#callBack_items");
	//Variables declaration
	var name = a.find('.name').val();
	var phonet = a.find('.phonet').val();
	var phonen = a.find('.phonen').val();
	var iname = a.find('.itemName').val();
	var iprice = a.find('.itemPrice').val();
	
	//Validation process begins
	if( name == "" )
	{
		a.find('.name').css('box-shadow', '0 0 10px red inset');
		a.find('.name').css('border', '1px solid red');
		a.find(".name").focus();
		return false;
	}
	
	else if( phonet == "" || phonen == "" )
	{
		
		if( phonen == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonen').css('box-shadow', '0 0 10px red inset');
			a.find('.phonen').css('border', '1px solid red');
			a.find(".phonen").focus();
		} else {
			a.find('.phonen').removeAttr('style');
		}
		
		if( phonet == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonet').css('box-shadow', '0 0 10px red inset');
			a.find('.phonet').css('border', '1px solid red');
			a.find(".phonet").focus();
		} else {
			a.find('.phonet').removeAttr('style');
		}
	}
	else
	{
		a.find('.name').removeAttr('style');
		a.find('.phonet, .phonen').removeAttr('style');
		var dataString = "name=" + name +  "&phonet=" + phonet +  "&phonen=" + phonen +  "&iname=" + iname +  "&iprice=" + iprice;
		
		$.ajax({
			type: "POST",
			url: "modules/vpb_email_sender_items.php",
			data: dataString,
			cache: false,
			beforeSend: function() 
			{
				a.parent().find(".miniOverlay").fadeIn();
			},
			success: function(response)
			{
				var response_brought = response.indexOf('vpb_sent');
				if (response_brought != -1) 
				{
					a.each(function(){
						this.reset();
					});
					
					a.find('.name').val('');
					a.find('.phone').val('');					
					jQuery(".feedBackBox").fadeOut();					
					jQuery(".thankBox").fadeIn();			
					
					setTimeout(' jQuery(".thankBox, .overlay").fadeOut();', 5000);					
				}
				else
				{					
					a.each(function(){
						this.reset();
					});					
					a.parent().find(".errorBox, .fileBox i, .miniOverlay").fadeOut();
				}
			}
		});

	}
}

//This function sends all emails
function vpb_send_email_product() 
{
	var a = $("#callBack_product");
	//Variables declaration
	var name = a.find('.name').val();
	var phonet = a.find('.phonet').val();
	var phonen = a.find('.phonen').val();
	var iname = a.find('.itemName').val();
	var iprice = a.find('.itemPrice').val();
	
	//Validation process begins
	if( name == "" )
	{
		a.find('.name').css('box-shadow', '0 0 10px red inset');
		a.find('.name').css('border', '1px solid red');
		a.find(".name").focus();
		return false;
	}
	
	else if( phonet == "" || phonen == "" )
	{
		
		if( phonen == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonen').css('box-shadow', '0 0 10px red inset');
			a.find('.phonen').css('border', '1px solid red');
			a.find(".phonen").focus();
		} else {
			a.find('.phonen').removeAttr('style');
		}
		
		if( phonet == "" ) {
			a.find('.name').removeAttr('style');
			a.find('.phonet').css('box-shadow', '0 0 10px red inset');
			a.find('.phonet').css('border', '1px solid red');
			a.find(".phonet").focus();
		} else {
			a.find('.phonet').removeAttr('style');
		}
	}
	else
	{
		a.find('.name').removeAttr('style');
		a.find('.phonet, .phonen').removeAttr('style');
		var dataString = "name=" + name +  "&phonet=" + phonet +  "&phonen=" + phonen +  "&iname=" + iname +  "&iprice=" + iprice;
		
		$.ajax({
			type: "POST",
			url: "modules/vpb_email_sender_items.php",
			data: dataString,
			cache: false,
			beforeSend: function() 
			{
				a.parent().find(".miniOverlay").fadeIn();
			},
			success: function(response)
			{
				var response_brought = response.indexOf('vpb_sent');
				if (response_brought != -1) 
				{
					a.each(function(){
						this.reset();
					});
					
					a.find('.name').val('');
					a.find('.phone').val('');					
					jQuery(".feedBackBox").fadeOut();					
					jQuery(".thankBox").fadeIn();			
					
					setTimeout(' jQuery(".thankBox, .overlay").fadeOut();', 5000);					
				}
				else
				{					
					a.each(function(){
						this.reset();
					});					
					a.parent().find(".errorBox, .fileBox i, .miniOverlay").fadeOut();
				}
			}
		});

	}
}